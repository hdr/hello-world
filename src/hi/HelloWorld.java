package hi;

import java.util.*;

public class HelloWorld {
	
	
	public static int suma(int a, int b) {
		int c = a + b;
		return c;
	}
	
	public static String peronista() {
		return "Viva Perón";
	}
	
	public static void decirMensaje(String frase) {
		System.out.println(frase);
		return;
	}
	

	public static void main(String[] args) {

		int a = 626;

		double x = 3.1415;
		boolean b = true;
		String frase = "Viva Perón";
		char riBer = 'B';

		System.out.println("el valor de riBer es " + riBer);

		System.out.println("el valor de a es " + a);
		System.out.println("el valor de b es " + b);
		System.out.println("la frase es " + frase);
		
		if (a == 626) {
			System.out.println("Aguante Cristina!");
		} else {
			System.out.println("Viva Perón");
		}
		
		// a == 0 && b == 1
		// coso > 10 || algo < -1
		// !coso
		// n != 625
		
		int j = 0;
		while (j < 10) {
			System.out.println(j);
			j++;
		}
		
		System.out.println("con el for");
		
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		}
		
		// cantidad = input(int("ingresá la cantidad"))
				
		int cantidad;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("ingresá la cantidad: ");

		cantidad = input.nextInt();
		
		System.out.println("la cantidad es " + cantidad);

		
		int resultado = suma(3, 5);
		System.out.println("el resultado es " + suma(3, 5));
		System.out.println(peronista());
		decirMensaje("Aguante Cristina");
		input.close();
		
		Random r = new Random();
		System.out.println(r.nextInt());

		

	}

}
