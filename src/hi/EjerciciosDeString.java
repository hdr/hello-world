package hi;

public class EjerciciosDeString {

	public static int cantidadDeE(String s) {
		int count = 0;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == 'e') {
				count++;
			}
		}

		return count;
	}

	public static void main(String[] args) {
		
		System.out.println(cantidadDeE("elefante"));

	}

}
